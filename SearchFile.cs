﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Delegates
{
    internal class SearchFile
    {
        private readonly string _filePath;
        public SearchFile(string filePath)
        {
            _filePath = filePath;
        }

        public event EventHandler<FileFoundArgs> FileFound;
        public void Search(CancellationToken token)
        {
            while(token.IsCancellationRequested == false)
            {
                foreach(var file in Directory.EnumerateFiles(_filePath))
                {
                    RaiseFileFound(file);
                }
            }
        }
        private void RaiseFileFound (string file) => FileFound?.Invoke(this, new FileFoundArgs(file));
    }
}
