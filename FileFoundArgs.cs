﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    public class FileFoundArgs:EventArgs
    {
        public string FileName { get; set; }
        public FileFoundArgs(string fileName)
        {
            FileName = fileName;
        }
    }
}
