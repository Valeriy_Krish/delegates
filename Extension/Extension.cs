using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Delegates;

public static class Extension
{
    public static T GetMax<T>(this IEnumerable<T> collection, Func<T, float> getParameter) where T : class
    {
        var max = collection.FirstOrDefault();

        for (var i = 1; i < collection.Count(); i++)
        {
            var current = collection.ElementAtOrDefault(i);
            if (getParameter(current) > getParameter(max))
            {
                max = current;
            }
        }
        return max;
    }
}