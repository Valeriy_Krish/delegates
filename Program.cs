﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Linq;
using System.IO;

namespace Delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            var pets = new List<Pets>()
            {
                new Pets { Name="Boots", Age=4 },
                new Pets { Name="Whiskers", Age=1 },
                new Pets { Name="Barley", Age=8 }
            };
            var maxEl = pets.GetMax<Pets>(GenericToFloat);
            Console.WriteLine($"Максимальный элемент коллекции:{maxEl.Name}");

            Console.ReadKey();

            CancellationTokenSource cts = new ();
            cts.CancelAfter (TimeSpan.FromSeconds(15 ));
            CancellationToken token = cts.Token;

            string path = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory);

            var fileSearch = new SearchFile(path);
            int fileFound = 0;

            fileSearch.FileFound  += (sender, args) =>
            {
                Console.WriteLine(args.FileName);
                fileFound++;
            };
            fileSearch.Search(token);
        }
        static float GenericToFloat<T>(T element) where T : class
        {
            float res = 0;
            var list = new List<object>();
            if (typeof(T) == typeof(Pets))
            {
                PropertyInfo[] properties = element.GetType().GetProperties();
                foreach (var prop in properties)
                {
                    PropertyInfo property = prop;
                    var type = property.GetValue(element);
                    list.Add(type);
                }
                res = list[0].ToString().Length + (int)list[1];
            }
            return res;
        }
    }
}
